package Task4;
import java.util.List;
import java.util.LinkedList;

public class KUnit {
    private static List<String> checks;
    private static int checksMade = 0;
    private static int passedChecks = 0;
    private static int failedChecks = 0;

    private static void addToReport(String txt) {
        if (checks == null) {
            checks = new LinkedList<>();
        }
        checks.add(String.format("%04d: %s", checksMade++, txt));
    }

    public static void checkEquals(double value1, double value2) {
        if (value1 == value2) {
            addToReport(String.format("  %2f == %2f", value1, value2));
            passedChecks++;
        } else {
            addToReport(String.format("* %2f == %2f", value1, value2));
            failedChecks++;
            log(String.format("Check failed: %2f == %2f", value1, value2));
        }
    }

    public static void checkNotEquals(double value1, double value2) {
        if (value1 != value2) {
            addToReport(String.format("  %2f != %2f", value1, value2));
            passedChecks++;
        } else {
            addToReport(String.format("* %2f != %2f", value1, value2));
            failedChecks++;
            log(String.format("Check failed: %2f != %2f", value1, value2));
        }
    }

    public static void report() {
        log(String.format("%d checks passed", passedChecks));
        log(String.format("%d checks failed", failedChecks));
        log("");

        for (String check : checks) {
            log(check);
        }
    }

    private static void log(String message) {
        System.out.println(message);
    }
}
