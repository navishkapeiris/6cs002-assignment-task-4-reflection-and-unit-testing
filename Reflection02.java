package Task4;
public class Reflection02 {
    public static void main(String[] args) {
        Simple s = new Simple();
        s.squareA();
        //s.squareB(); //if you uncomment this you will get a compiler error
        double a = s.getA();
         //double b = s.getB(); //if you uncomment this you will get a compiler error
        System.out.println("s=" + s);
    }
}
